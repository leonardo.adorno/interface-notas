/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkginterface.notas.certo;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Leonardo
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label media;
    
    @FXML
    private TextField nome;

    @FXML
    private TextField nota1;
    
    @FXML
    private TextField nota2;
    
    @FXML
    private TextField nota3;
    
   
    
    @FXML
    private TextField historico;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        Aluno aluno = new Aluno(nome.getText());
        aluno.setNotasAtuais(0, Double.parseDouble(nota1.getText()));
        aluno.setNotasAtuais(1, Double.parseDouble(nota2.getText()));
        aluno.setNotasAtuais(2, Double.parseDouble(nota3.getText()));
        aluno.setPesosAtuais(0, 4);
        aluno.setPesosAtuais(1, 4);
        aluno.setPesosAtuais(2, 2);
        media.setText(Double.toString(aluno.getMedia(aluno.getPesosAtuais())));
        //trocaTela()
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
//    Scanner input = new Scanner(System.in);
//        
//        Aluno vetAlunos[] = new Aluno[50];
//        int quantAlunos=0;
//        double vetorPesos[] = new double[3]; 
//        int i;
//        do{
//            System.out.println("Informe os pesos do trabalho e das provas somando 10: ");
//            for(i=0;i<3;i++){
//                vetorPesos[i] = input.nextDouble();
//            
//            }
//        }while(vetorPesos[0] + vetorPesos[1] + vetorPesos[2] != 10);
//        
//        
//         int op;
//        do{
//            System.out.println("1-Cadastra Alunos\n2-Calcular Trimestre\n3-Mostrar Historico\n4-Mostrar Turma\n5-Sair ");
//            op = input.nextInt(); input.nextLine();
//            switch(op){
//            case 1:
//                //PORQUE O PRÓXIMO ALUNO É O LOGO DEPOIS DO ÚLTIMO
//                // ENTÃO É O VET[QUANTIDADE]
//                System.out.println("Digite o seu nome: ");
//                String nomeDoAluno = input.nextLine();
//                vetAlunos[quantAlunos] = new Aluno( nomeDoAluno );
////                vetAlunos[quantAlunos].nome = input.nextLine();
//                        
//                System.out.println("Digite as suas notas atuais: ");
//                 int x;
//                 for(x=0;x<3;x++){
//                    vetAlunos[quantAlunos].notasAtuais[x] = input.nextDouble();
//                    input.nextLine();
//                 }
//                 
//                 
//                 System.out.println("Digite a sua nota do mesmo trimestre do ano passado: ");
//                 vetAlunos[quantAlunos].notaHist = input.nextDouble();
//                 
//                  int z;
//                    for(z=0;z<3;z++){
//                        if(vetAlunos[quantAlunos].notasAtuais[z] == 0){
//                            vetAlunos[quantAlunos].notasAtuais[z] = 0.1;
//                        }
//                    }
//                 
//                 quantAlunos++;
//                 
//                 
//                 
//                break;
//                
//            case 2:
//                int y;
//                double soma;
//                for(y=0;y<quantAlunos;y++){
//                   
//                    
//                    //10/(PESOTRAB/trabalho+ PSEOPROV1/p1+PESOPROV2/p2) 
//                    
//                    System.out.println("Soma da: " + vetAlunos[y].nome + " eh: " + vetAlunos[y].getMedia(vetorPesos));
//                   
//                    
//                }
//                break;
//                
//            case 3:
//                int k=0;
//                int num;
//                for(k=0;k<quantAlunos;k++){
//                   System.out.println("Nome: " + vetAlunos[k].nome + "\tNumero: " + k);
//                }
//                System.out.println("De qual aluno você quer saber a comparação? (Digite o numero!)");
//                num = input.nextInt();
//                System.out.println("Nome: " + vetAlunos[num].nome + " \tMedia: "
//                        + vetAlunos[num].getMedia(vetorPesos) + " \tHistorico: " + vetAlunos[num].notaHist);
//                break;
//                
//            case 4:
//                int j=0;
//                for(j=0;j<quantAlunos;j++){
//                   System.out.println("Nome: " + vetAlunos[j].nome + "\tNota: " + vetAlunos[j].getMedia(vetorPesos)); 
//                }
//                
//                break;
//                
//            }
//            
//            
//        }while(op!=5);
//    
}
