/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkginterface.notas.certo;

/**
 *
 * @author Leonardo
 */
public class Aluno {
    
    String nome;
    double[] notasAtuais = new double[3];
    double[] pesosAtuais = new double[3];
    double notaHist;

    public double[] getPesosAtuais() {
        return pesosAtuais;
    }

    public void setPesosAtuais(int i, double pesosAtuais) {
        this.pesosAtuais[i] = pesosAtuais;
    }
 
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double[] getNotasAtuais() {
        return notasAtuais;
    }

    public void setNotasAtuais(int i, double notasAtuais) {
        this.notasAtuais[i] = notasAtuais;
    }

    public double getNotaHist() {
        return notaHist;
    }

    public void setNotaHist(double notaHist) {
        this.notaHist = notaHist;
    }
    

    public Aluno(String nome) {
        this.nome = nome;
    }

    double getMedia(double vetorPesos[]) {
        double soma;
        soma = vetorPesos[0] / this.notasAtuais[0];
        soma += vetorPesos[1] / this.notasAtuais[1];
        soma += vetorPesos[2] / this.notasAtuais[2];

        soma = 10.0 / soma;
        return soma;
    }
}
